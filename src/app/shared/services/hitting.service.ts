import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HittingService {
  constructor(private httpClient: HttpClient) {}

  public getHello(): Observable<string> {
    return this.httpClient
      .get('https://localhost:60547/hello')
      .pipe(map((response: any) => response.message));
  }

  public uploadFiles(formData: FormData): Observable<Blob> {
    return this.httpClient.post(
      'https://localhost:60547/uploadFile',
      formData,
      { responseType: 'blob' }
    );
  }
}

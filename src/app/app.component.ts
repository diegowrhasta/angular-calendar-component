import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import {
  CalendarOptions,
  DateSelectArg,
  EventAddArg,
  EventClickArg,
  EventHoveringArg,
} from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, { DateClickArg } from '@fullcalendar/interaction';
import { HittingService } from './shared/services/hitting.service';
import { Observable, fromEvent } from 'rxjs';
import esLocale from '@fullcalendar/core/locales/es';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('calendar') calendar?: FullCalendarComponent;

  private readonly allowedTypes = ['image/jpeg', 'image/pjpeg', 'image/png'];

  constructor(private hittingService: HittingService) {}

  public response?: Observable<string>;

  public uploadFileResponse?: Observable<any>;

  public imageString = '';

  public fileInput?: HTMLElement;

  public calendarOptions: CalendarOptions = {
    dayMaxEvents: true,
    selectable: true,
    select: this.onDragAndDropSelection.bind(this),
    initialView: 'dayGridMonth',
    eventAdd: this.onEventAdded.bind(this),
    eventMouseEnter: this.onEventMouseEnter.bind(this),
    eventMouseLeave: this.onEventMouseLeave.bind(this),
    dateClick: this.checkDateClick.bind(this),
    eventClick: this.checkEventClick.bind(this),
    plugins: [dayGridPlugin, interactionPlugin],
    locale: esLocale,
    titleFormat: this.formatTitle.bind(this),
  };

  ngOnInit(): void {
    console.log('hello');
  }

  ngAfterViewInit(): void {
    this.calendar!.events = [
      {
        // Date string should be YYYY-MM-DD
        title: 'Mokiz maderfaker', // a property!
        start: '2024-01-30', // a property!
        end: '2024-02-01', // a property! ** see important note below about 'end' **
      },
    ];

    this.fileInput = document.getElementById('file')!;
    fromEvent(this.fileInput, 'change').subscribe(this.onFileUpload.bind(this));
  }

  public checkDateClick(info: DateClickArg): void {
    console.log(info);
  }

  public checkEventClick(info: EventClickArg): void {
    console.log(info);
    console.log(info.event._def.title);
  }

  public onEventMouseEnter(info: EventHoveringArg): void {
    info.el.style.backgroundColor = '#FFFF00';
  }

  public onEventMouseLeave(info: EventHoveringArg): void {
    info.el.style.backgroundColor = 'var(--fc-event-bg-color)';
  }

  public onDragAndDropSelection(info: DateSelectArg): void {
    console.log(info);
  }

  public onEventAdded(info: EventAddArg): void {
    console.log(info);
  }

  public addEvent(): void {
    const internalCalendar = this.calendar?.getApi();
    const eventColor =
      Math.floor(Math.random() * 100) % 2 === 0 ? 'purple' : 'blue';
    internalCalendar?.addEvent({
      title: 'The Title', // a property!
      start: '2024-01-30', // a property!
      end: '2024-02-01', // a property! ** see important note below about 'end' **
      color: eventColor,
      extendedProps: {
        id: 1,
        name: 'Diegow',
      },
    });

    this.response = this.hittingService.getHello();
  }

  public onFileUpload(event: any): void {
    const element = event.target as HTMLInputElement;
    const files = element.files;

    if (!!!files?.length) {
      return;
    }

    var formData = new FormData();

    let numberOfValidFiles = 0;
    for (let i = 0; i < files!.length; i++) {
      if (!this.allowedTypes.includes(files![i].type)) {
        console.log('Invalid type', files![i].type);
        continue;
      }

      numberOfValidFiles++;
      formData.append('fileToUpload', files![i]);
    }

    if (!numberOfValidFiles) {
      return;
    }
    
    this.uploadFileResponse = this.hittingService.uploadFiles(formData);

    this.uploadFileResponse.subscribe((data) => {
      const reader = new FileReader();
      reader.readAsDataURL(data);

      reader.onloadend = () => this.onFileRead.bind(this)(reader);
    });
  }

  private onFileRead(reader: FileReader): void {
    var base64data = reader.result;
    this.imageString = base64data!.toString();
  }

  private formatTitle(date: { [key: string]: any }): string {
    const dateInstance = date['date'].marker as Date;
    const initialString = dateInstance.toLocaleDateString('es-BO', {
      year: 'numeric',
      month: 'long',
    });
    const splittedString = initialString.split(' ');
    splittedString[0] =
      splittedString[0].charAt(0).toUpperCase() +
      splittedString[0].substring(1).toLowerCase();

    return splittedString.join(' ');
  }
}
